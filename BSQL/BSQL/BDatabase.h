#if !defined(BDAT_H)
#define DBAT_H
#include "rowdata.h"
#include <time.h>
#include <iomanip>
#include <string>
#include <assert.h> 
#include <array>
#include "Schema.h"
#include "StringSplit.h"
#include "IndexableTree.h"
#include <fstream>
#include <algorithm>
#include "PerformanceMonitor.h"
#include <iostream>
#include <vector>
#include "IndexableArray.h"

using namespace std;

class BDatabase{
private:
	Schema *schema;
	bool usingHash;
	int insertSteps_ = 0;
	int deleteSteps = 0;
	int searchSteps = 0;
	int collisions = 0;
	IndexableArray *indexable = NULL; 
	IndexableTree *indexableTree = NULL;
	long incHash(char *key, IndexableArray*);
	vector<int> incSearch(char*, IndexableArray*, string search, int column);
	float loadFactor = 0.1; //keep track of the load factor in case tables need resized later
	clock_t t1, t2; //used for calculating execution of SQL statements
	string StringToUpper(string strToConvert);
public:
	BDatabase(string TABLENAME, bool useHash);
	~BDatabase();
	int execSQL(string command); //take in an sql statement as a string to parse and execute, returns negative number
								 //if statement fails, returns postive number which is the milliseconds taken to execute 
								//if successful. 
};
#endif