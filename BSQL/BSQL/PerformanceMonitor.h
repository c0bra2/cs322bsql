#if !defined(PERFORM_H)
#define PERFORM_H
#include <time.h>

extern int insertSteps;
extern int searchSteps;
extern clock_t time1;
extern clock_t time2;
#endif