#include <iostream>
#include <string>
#include <algorithm>
#include <windows.h>
#include "BDatabase.h"
using namespace std;

string StringToUpper(string strToConvert);
string prompt();
int main(){
	string statement;

	//resize window 
	system("mode 150,50");   //Set mode to ensure window does not exceed buffer size
	SMALL_RECT WinRect = { 0, 0, 150, 50 };   //New dimensions for window in 8x12 pixel chars
	SMALL_RECT* WinSize = &WinRect;
	SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, WinSize);   //Set new size for window

	BDatabase datab("EXAMPLE", true); //open example table and use hashIndexing

	//display prompt and wait for SQL statements
	while (true){
		statement = prompt();
		datab.execSQL(statement);
	}
	return 0;
}

string StringToUpper(string strToConvert)
{
	std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

	return strToConvert;
}

string prompt(){
	string userin;
	cout << "BSQL> ";
	getline(cin, userin);
	return StringToUpper(userin);
}