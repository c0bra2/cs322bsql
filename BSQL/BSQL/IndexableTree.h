#if !defined (INDEXABLET_H)
#define INDEXABLET_H
#include "rowdata.h"
#include "avlTree.h"
#include "Schema.h"
#include <vector>
using namespace std;

class IndexableTree{
private:
	int key;
	IndexableTree *next;
public:
	int getKey();
	AVLTreeType *tree = new AVLTreeType;
	node *root = NULL;
	void deleteRows(string searchKey);
	void deleteRows(string searchKey, int columnNum);
	void selectALL(Schema* schobj, vector<string> columns);
	vector<rowdata*> getRows(string searchKey);
	IndexableTree *getNext();
	void setRow(rowdata *row);
	void setNext(IndexableTree *next);
	~IndexableTree();
	IndexableTree(int);
};
#endif