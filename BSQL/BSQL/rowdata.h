#if !defined(ROW_H)
#define ROW_H
#include <assert.h>
#include "StringSplit.h"
#include "DataEntry.h"
#include <vector>
using namespace std;
class rowdata{
private:
	DataEntry *row = NULL;
public:
	rowdata(string data);
	string getRowData(int);
	vector<rowdata*> duplicateEntry;
	int getLength();
	bool deleted = false;
	~rowdata();
};
#endif