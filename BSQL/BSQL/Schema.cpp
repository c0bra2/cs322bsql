#include "Schema.h"

Schema::Schema(string table){
	//read in the file 
	string line;
	StringSplit splitTool = StringSplit(3, ',');
	ifstream schemaFile(table + "/" + "schema.csv");
	int element = 0;
	DataEntry *entry;
	if (schemaFile.is_open()){
		while (getline(schemaFile, line)){
			line = StringToUpper(line);
			splitTool.newString(line);
			for (int i = 0; i < splitTool.tokens; i++){
				if (i == 0){
					entry = Column_Name;
					//title
					if (entry != NULL){
						while (entry->getNext() != NULL)
						{
							entry = entry->getNext();
						}
						entry->setNext(new DataEntry(element, splitTool.tokenList[i]));

					}
					else{
						Column_Name = new DataEntry(element, splitTool.tokenList[i]);
					}
				}
				else if (i == 1){
					entry = Data_Type;
					//datatype
					if (entry != NULL){
						while (entry->getNext() != NULL)
						{
							entry = entry->getNext();
						}
						entry->setNext(new DataEntry(element, splitTool.tokenList[i]));

					}
					else{
						Data_Type = new DataEntry(element, splitTool.tokenList[i]);
					}
				}
				else{
					entry = Is_Index;
					//indexable column
					if (entry != NULL){
						while (entry->getNext() != NULL)
						{
							entry = entry->getNext();
						}
						entry->setNext(new DataEntry(element, splitTool.tokenList[i]));
						if (splitTool.tokenList[i] == "YES")
							indexablecolumns++;

					}
					else{
						Is_Index = new DataEntry(element, splitTool.tokenList[i]);
						if (splitTool.tokenList[i] == "YES")
							indexablecolumns++;
					}
				}
			}
			element++;
		}
		columnNum = element;
		schemaFile.close();
	}
	else{
		tableFound = false; //the table was not found, or a file read error 
	}
}

int Schema::getLength(){
	int count = 0;
	DataEntry *data = Column_Name;
	if (data == NULL)
		return 0;
	while (data->getNext() != NULL){
		count++;
		data = data->getNext();
	}
	return count;
}

int Schema::findColumnName(string columnName){
	DataEntry *entry = Column_Name;
	while (entry->getNext() != NULL && entry->getValue() != columnName){
		entry = entry->getNext();
	}
	if (entry->getValue() == columnName){
		return entry->getKey();
	}
	return 0;
}

string Schema::findColumnName(int index){
	DataEntry *entry = Column_Name;
	while (entry->getNext() != NULL && entry->getKey() != index){
		entry = entry->getNext();
	}
	if (entry->getKey() == index){
		return entry->getValue();
	}
	return "";
}

string Schema::findDataType(int index){
	DataEntry *entry = Data_Type;
	while (entry->getNext() != NULL && entry->getKey() != index){
		entry = entry->getNext();
	}
	if (entry->getKey() == index){
		return entry->getValue();
	}
	return "";
}

string Schema::findIsIndex(int index){
	DataEntry *entry = Is_Index;
	while (entry->getNext() != NULL && entry->getKey() != index){
		entry = entry->getNext();
	}
	if (entry->getKey() == index){
		return entry->getValue();
	}
	return "";
}
Schema::~Schema(){
	//deallocate memory
	DataEntry *prevEntry = Column_Name;
	while (Column_Name != NULL){
		prevEntry = Column_Name;
		Column_Name = Column_Name->getNext();
		delete prevEntry;
	}
	while (Data_Type != NULL){
		prevEntry = Data_Type;
		Data_Type = Data_Type->getNext();
		delete prevEntry;
	}
	while (Is_Index != NULL){
		prevEntry = Is_Index;
		Is_Index = Is_Index->getNext();
		delete prevEntry;
	}
}

string Schema::StringToUpper(string strToConvert)
{
	std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

	return strToConvert;
}