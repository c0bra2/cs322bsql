#include "IndexableArray.h"

IndexableArray::IndexableArray(int k){

	table.resize(ARRAYSIZE, NULL);
	this->key = k;
	this->next = NULL;
	largePrimes[0] = 31393;
	largePrimes[1] = 70289;
	largePrimes[2] = 141461; 
	largePrimes[3] = 280583; 
	largePrimes[4] = 566617;
	largePrimes[5] = 1002577;
};

void IndexableArray::resizeHashTable()
{
	int hashIndex;
	if (currentPrime < LARGEP){
		int newSize = largePrimes[currentPrime];
		vector<rowdata*> tempVector;
		rowdata* entry = NULL;
		tempVector.resize(newSize, NULL);
		for (int i = 0; i < ARRAYSIZE; i++){
			if (table[i] != NULL){
				entry = table[i];
				hashIndex = reHash((char*)entry->getRowData(i).c_str());
				tempVector[hashIndex] = entry;
			}
			table.clear();
			table = tempVector;
		}
		currentPrime++;
		cout << "TABLESIZE changed from " << ARRAYSIZE << " to " << newSize << " and table rehashed!" << endl;
		ARRAYSIZE = newSize;
	}
}

int IndexableArray::getKey(){
	return key;
}

long IndexableArray::reHash(char *key){
	unsigned long h;
	int hashSize = getDataSize();
	int startIndex;
	h = 5;
	int count = 0;
	for (int i = 0; *key; i++)
	{
		h *= *key++;
		h += count;
		h *= 19;
		count++;
	}

	h = (h % hashSize);
	if (getRow(h) != NULL)
	{
		startIndex = h;
		//start linear probing algorithm
		for (int i = startIndex + 1; i != startIndex; i++)
		{
			if (i == hashSize)
				i = 0; //i has reached the end of the hashtable indexes, and must be set to zero to begin searching
			//from index zero instead. from this point we will continue to increment i until it becomes 
			//equal to the starting index, or finds an empty spot.

			if (getRow(i) == NULL)
				return(i);
		}
		//if this function hasn't returned we know that the hashtable is full. 
		return (-1); //when this function returns -1, it means the hashtable is full. 
	}
	else{
		return (h); //this return statement executes it means that a successful search has occured. 
	}
}

rowdata* IndexableArray::getRow(int k){
	return this->table[k];
}

float IndexableArray::getLoadFactor()
{
	return (float)entries / (float)ARRAYSIZE;
}

void IndexableArray::setRow(int k, rowdata *row){
	this->table[k] = row;
	entries++;
}

void IndexableArray::setNext(IndexableArray *n){
	this->next = n;
}

IndexableArray* IndexableArray::getNext(){
	return this->next;
}

int IndexableArray::getDataSize(){
	return ARRAYSIZE;
}

IndexableArray::~IndexableArray(){
	//CLEANUP
	//for (int i = 0; i < ARRAYSIZE; i++)
	//	if (table[i] != NULL){
	//		rowdata *entry = table[i];
	//		delete entry;
	//	}
	//delete[] table;
}