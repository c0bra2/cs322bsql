#include "BDatabase.h"

BDatabase::BDatabase(string TABLENAME, bool useHash)
{
	t1 = clock();
	string line;
	ifstream dataFile(TABLENAME + "/" + "data.csv");
	IndexableArray *entry;
	IndexableTree *treeEntry;
	int hashIndex;
	rowdata *data; 
	//load schema
	schema = new Schema(TABLENAME);
	assert(schema->indexablecolumns != 0); // There must be at least one indexable column, indexable columns can also be called
										  // primary keys
	usingHash = useHash;


	if (usingHash){
		//use hashing to store data

		//create hashtable for each indexable array
		for (int i = 0; i < schema->indexablecolumns; i++){
			entry = indexable;
			if (indexable != NULL){
				while (entry->getNext() != NULL){
					entry = entry->getNext();
				}
				entry->setNext(new IndexableArray(i));
			}
			else{
				indexable = new IndexableArray(i);
			}
		}

		if (dataFile.is_open()){
			while (getline(dataFile, line)){
				data = new rowdata(StringToUpper(line));
				assert(data->getLength() == schema->getLength());
				entry = indexable;
				for (int i = 0; i < schema->indexablecolumns; i++){
					if (entry->getLoadFactor() > 0.4){
						entry->resizeHashTable();
					}
					hashIndex = incHash((char*)data->getRowData(i).c_str(), entry);
					entry->setRow(hashIndex, data);
					entry = entry->getNext();
				}
			}
		}
	}
	else{
		//use binary trees instead
		//create a tree for every indexable column
		for (int i = 0; i < schema->indexablecolumns; i++){
			treeEntry = indexableTree;
			if (indexableTree != NULL){
				while (treeEntry->getNext() != NULL){
					treeEntry = treeEntry->getNext();
				}
				treeEntry->setNext(new IndexableTree(i));
			}
			else{
				indexableTree = new IndexableTree(i);
			}
		}
		
		//read in data
		if (dataFile.is_open()){
			while (getline(dataFile, line)){
				data = new rowdata(StringToUpper(line));
				assert(data->getLength() == schema->getLength());
				treeEntry = indexableTree;
				for (int i = 0; i < schema->indexablecolumns; i++){
					treeEntry->setRow(data);
					treeEntry = treeEntry->getNext();
				}
			}
		}
	}
	//int sum = 0;
	dataFile.close();
	//t2 = clock();
	//cout << "Statement executed in " << ((float)t2 - (float)t1) << "ms" << endl << endl;

	//treeEntry = indexableTree;
	//for (int i = 0; i < schema->indexablecolumns; i++){
	//	sum += treeEntry->tree->insertSteps;
	//	treeEntry = treeEntry->getNext();
	//}
	//cout << "insert steps: " << sum << endl;

	////cout << "insert steps: " << insertSteps_ << endl;
	////cout << "collisions: " << collisions << endl;
	//t2 = clock();

}

long BDatabase::incHash(char *key, IndexableArray *indexarray){
	unsigned long h;
	int hashSize = indexarray->getDataSize();
	int startIndex;
	h = 5;
	int count = 0;
	insertSteps_++;
	for (int i = 0; *key; i++)
	{
		h *= *key++;
		h += count;
		h *= 19;
		count++;
	}

	h = (h % hashSize);
	if (indexarray->getRow(h) != NULL)
	{
		collisions++;
		startIndex = h;
		//start linear probing algorithm
		for (int i = startIndex + 1; i != startIndex; i++)
		{
			insertSteps_++;
			if (i == hashSize)
				i = 0; //i has reached the end of the hashtable indexes, and must be set to zero to begin searching
			//from index zero instead. from this point we will continue to increment i until it becomes 
			//equal to the starting index, or finds an empty spot.

			if (indexarray->getRow(i) == NULL)
					return(i);
		}
		//if this function hasn't returned we know that the hashtable is full. 
		return (-1); //when this function returns -1, it means the hashtable is full. 
	}
	else{
		return (h); //this return statement executes it means that a successful search has occured. 
	}
}

vector<int> BDatabase::incSearch(char *key, IndexableArray *indexarray, string search, int column){
	unsigned long h;
	vector<int> res;
	int elementsInVector = 0;
	res.resize(0);
	int startIndex = 0;
	h = 5;
	int count = 0;
	searchSteps++;
	for (int i = 0; *key; i++)
	{
		h *= *key++;
		h += count;
		h *= 19;
		count++;
	}

	h = (h % indexarray->getDataSize());
	if (indexarray->getRow(h) == NULL){
		searchSteps++;
		return res;
	}

	if (indexarray->getRow(h)->getRowData(column) == search && indexarray->getRow(h)->deleted != true){
		res.resize(elementsInVector + 1);
		res[elementsInVector] = h;
		elementsInVector++;
		searchSteps++;
	}
	startIndex = h;
	//start linear probing algorithm
	for (int i = startIndex + 1; i != startIndex; i++)
	{
		searchSteps++;
		if (i == indexarray->getDataSize())
			i = 0; //i has reached the end of the hashtable indexes, and must be set to zero to begin searching
		//from index zero instead. from this point we will continue to increment i until it becomes 
		//equal to the starting index, or finds an empty spot.
		if (indexarray->getRow(i) == NULL){
			return res;
		}
		if (indexarray->getRow(i)->getRowData(column) == search && indexarray->getRow(i)->deleted != true){
			res.resize(elementsInVector + 1);
			res[elementsInVector] = i;
			elementsInVector++;
		}
				
	}
	return res; 
	
}

int BDatabase::execSQL(string command){
	t1 = clock();
	command = StringToUpper(command);
	string instruction = "";
	string condition = "";
	string columnToCompare = "";
	vector<string> columns;
	vector <int> searchResults;
	IndexableArray *indexable_;
	IndexableTree *indexable_t;
	int records = 0;
	rowdata *entry;
	int columnNum = 0; //how many strings are in the columns vector
	columns.resize(0);
	int hashIndex;
	int i = 0; //keep track of where we are in the string
	string temp = "";
	
	//check for 1 token commands such as save; and clear;
	if (command == ""){
		return -1;
	}
	else if (command == "SAVE;"){

	}
	else if (command == "CLEAR;"){
		system("cls");
		return 0;
	}
	else if (command == "SELECT;"){
		return -1;
	}
	else if (command == "DELETE;"){
		return -1;
	}
	else if (command == "INSERT;")
		return -1;
	else if (command == "EXIT;"){
		exit(0);
	}

	if (usingHash){
	
		//begin parsing
		for (i = 0; i < command.length(); i++){
			if (command[i] == ' '){
				if (i + 1 < command.length()){
					for (int j = (i + 1); j < command.length(); j++){
						temp += command[j];
					}
					i = command.length();
					command = temp;
				}
				else{
					cout << "Malformed SQL statement!" << endl;
					return -1;
				}
			}
			else{
				instruction += command[i];
			}
		}

		if (command[command.length() - 1] != ';')
		{
			cout << "Missing ; delimiter" << endl;
			return -1;
		}

		//determine the instruction that was used
		if (instruction == "SELECT")
		{
			//get columns and store them in a vector
			temp = "";
			for (i = 0; i < command.length(); i++){
				if ((command[i] == ' ' || command[i] == ';') && command[i - 1] != ','){
					columns.resize(columnNum);
					columns.push_back(temp);
					temp = "";
					i++;
					columnNum++;
					break;
				}
				else if (command[i] == ','){
					columns.resize(columnNum);
					columns.push_back(temp);
					temp = "";
					columnNum++;
				}
				else if (command[i] != ' '){
					temp += command[i];

				}
			}
			temp = "";
			for (; i < command.length(); i++){
				temp += command[i];
			}
			command = temp;



			bool inCondition = false; //has the program encountered a \' symbol in the where statement?
			temp = "";
			columnNum = 0; //being used as a general int value placeholder throughout the rest of the program
			//get where condition if one exist
			for (i = 0; i < command.length(); i++){
				if ((command[i] == ' ' || command[i] == ';') && !inCondition){
					columnNum++;
					if (columnNum == 2)
						columnToCompare = temp;
					else if (columnNum == 4){
						for (int j = 0; j < temp.length(); j++)
							condition += temp[j];
					}
					temp = "";
				}
				else if (command[i] == ' ' && inCondition)
					temp += ' ';
				else if (command[i] == '\'')
					inCondition = !inCondition;
				else
					temp += command[i];
			}

			i = 0;
			if (columnToCompare == "")
			{

				//no where statement
				cout << "\n";
				for (int j = 0; j < columns.size(); j++){
					cout << columns[j] << "\t     ";
				}
				cout << "\n\n";
				for (int j = 0; j < indexable->getDataSize(); j++){
					entry = indexable->getRow(j);
					if (entry != NULL && indexable->getRow(j)->deleted != true){
						for (int k = 0; k < columns.size(); k++){
							cout << entry->getRowData(schema->findColumnName(columns[k])) << "\t     ";
						}
						records++;
						cout << "\n";
					}
				}
				cout << "\n";
				cout << records << " records found." << endl;
			}
			else
			{
				searchSteps = 0;
				collisions = 0;
				//statement had a where clause
				cout << "\n";
				for (int j = 0; j < columns.size(); j++){
					cout << columns[j] << setw(15);
				}
				cout << "\n\n";
				//search for results 
				columnNum = schema->findColumnName(columnToCompare);
				indexable_ = indexable;
				for (int j = 0; j != columnNum; j++){
					indexable_ = indexable_->getNext();
				}
				searchResults = incSearch((char*)condition.c_str(), indexable_, condition, columnNum);
				records = searchResults.size();
				for (int k = 0; k < searchResults.size(); k++){
					entry = indexable_->getRow(searchResults[k]);
					for (int l = 0; l < columns.size(); l++){
						cout << entry->getRowData(schema->findColumnName(columns[l])) << "\t     ";
					}
					cout << "\n";
				}
				cout << "\n";
				cout << records << " records found." << endl;
				//cout << "Search Steps: " << searchSteps << endl;
			}
			//end If SELECT
		}
		if (instruction == "INSERT"){
			temp = "";
			//insertSteps_ = 0;
			bool inCondition = false;
			//strip whitespace outside outside single quotes
			for (i = 0; i < command.length(); i++){
				if (command[i] == ' ' && inCondition)
					temp += ' ';
				else if (command[i] == '\''){
					inCondition = !inCondition;
					temp += '\'';
				}
				else if (command[i] == ' ' && !inCondition);
				else
					temp += command[i];
			}
			command = temp;
			temp = "";
			//strip parenthesis, and single quotes
			for (i = 0; i < command.length(); i++)
				if (command[i] != '\'' && command[i] != '(' && command[i] != ')' && command[i] != ';'){
					temp += command[i];
				}

			entry = new rowdata(StringToUpper(temp));
			if (entry->getLength() != schema->getLength()){
				cout << "Error! Insert data does not match Schema" << endl;
				return -1;
			}
			indexable_ = indexable;
			for (int i = 0; i < schema->indexablecolumns; i++){
				hashIndex = incHash((char*)entry->getRowData(i).c_str(), indexable_);
				indexable_->setRow(hashIndex, entry);
				indexable_ = indexable_->getNext();
			}

			//cout << "Insert Steps: " << insertSteps_ << endl;
			cout << " 1 record added" << endl;
		}
		//end If INSERT

		if (instruction == "DELETE"){
			bool inCondition = false; //has the program encountered a \' symbol in the where statement?
			temp = "";
			columnNum = 0; //being used as a general int value placeholder throughout the rest of the program
			//get where condition if one exist
			for (i = 0; i < command.length(); i++){
				if ((command[i] == ' ' || command[i] == ';') && !inCondition){
					columnNum++;
					if (columnNum == 2)
						columnToCompare = temp;
					else if (columnNum == 4){
						for (int j = 0; j < temp.length(); j++)
							condition += temp[j];
					}
					temp = "";
				}
				else if (command[i] == ' ' && inCondition)
					temp += ' ';
				else if (command[i] == '\'')
					inCondition = !inCondition;
				else
					temp += command[i];
			}


			if (columnToCompare != "")
			{
				records = 0;
				//search for results 
				columnNum = schema->findColumnName(columnToCompare);
				indexable_ = indexable;
				for (int j = 0; j != columnNum; j++){
					indexable_ = indexable_->getNext();
				}
				searchResults = incSearch((char*)condition.c_str(), indexable_, condition, columnNum);
				for (int k = 0; k < searchResults.size(); k++){
					searchSteps++;
					indexable_->getRow(searchResults[k])->deleted = true;
					records++;
				}
				cout << "Delete Steps: " << searchSteps << endl;
				cout << records << " records deleted!" << endl;
			}
		}
		//end If DELETE
	}
	else{
		//begin parsing for using trees
		for (i = 0; i < command.length(); i++){
			if (command[i] == ' '){
				if (i + 1 < command.length()){
					for (int j = (i + 1); j < command.length(); j++){
						temp += command[j];
					}
					i = command.length();
					command = temp;
				}
				else{
					cout << "Malformed SQL statement!" << endl;
					return -1;
				}
			}
			else{
				instruction += command[i];
			}
		}

		if (command[command.length() - 1] != ';')
		{
			cout << "Missing ; delimiter" << endl;
			return -1;
		}

		//determine the instruction that was used
		if (instruction == "SELECT")
		{
			//get columns and store them in a vector
			temp = "";
			for (i = 0; i < command.length(); i++){
				if ((command[i] == ' ' || command[i] == ';') && command[i - 1] != ','){
					columns.resize(columnNum);
					columns.push_back(temp);
					temp = "";
					i++;
					columnNum++;
					break;
				}
				else if (command[i] == ','){
					columns.resize(columnNum);
					columns.push_back(temp);
					temp = "";
					columnNum++;
				}
				else if (command[i] != ' '){
					temp += command[i];

				}
			}
			temp = "";
			for (; i < command.length(); i++){
				temp += command[i];
			}
			command = temp;



			bool inCondition = false; //has the program encountered a \' symbol in the where statement?
			temp = "";
			columnNum = 0; //being used as a general int value placeholder throughout the rest of the program
			//get where condition if one exist
			for (i = 0; i < command.length(); i++){
				if ((command[i] == ' ' || command[i] == ';') && !inCondition){
					columnNum++;
					if (columnNum == 2)
						columnToCompare = temp;
					else if (columnNum == 4){
						for (int j = 0; j < temp.length(); j++)
							condition += temp[j];
					}
					temp = "";
				}
				else if (command[i] == ' ' && inCondition)
					temp += ' ';
				else if (command[i] == '\'')
					inCondition = !inCondition;
				else
					temp += command[i];
			}

			i = 0;
			if (columnToCompare == "")
			{

				//no where statement
				cout << "\n";
				for (int j = 0; j < columns.size(); j++){
					cout << columns[j] << "\t     ";
				}
				cout << "\n\n";
				
				/*indexableTree->selectALL(schema, columns);*/
			}
			else
			{
				//statement had a where clause
				cout << "\n";
				for (int j = 0; j < columns.size(); j++){
					cout << columns[j] << setw(15);
				}
				cout << "\n\n";
				//search for results 
				columnNum = schema->findColumnName(columnToCompare);
				indexable_t = indexableTree;
				for (int j = 0; j != columnNum; j++){
					indexable_t = indexable_t->getNext();
				}
				vector<rowdata*> sresult;
				sresult = indexable_t->getRows(condition);
				records = sresult.size();
				for (int k = 0; k < sresult.size(); k++){
					entry = sresult[k];
					for (int l = 0; l < columns.size(); l++){
						cout << entry->getRowData(schema->findColumnName(columns[l])) << "\t     ";
					}
					cout << "\n";
				}
				cout << "\n";
				cout << records << " records found." << endl;
				//cout << "Search Steps: " << indexable_t->tree->searchSteps << endl;
			}
			//end If SELECT
		}
		if (instruction == "INSERT"){

			//indexable_t = indexableTree;
			//for (int i = 0; i < schema->indexablecolumns; i++){
			//	indexable_t->tree->insertSteps = 0;
			//	indexable_t = indexable_t->getNext();
			//}

			temp = "";
			bool inCondition = false;
			//strip whitespace outside outside single quotes
			for (i = 0; i < command.length(); i++){
				if (command[i] == ' ' && inCondition)
					temp += ' ';
				else if (command[i] == '\''){
					inCondition = !inCondition;
					temp += '\'';
				}
				else if (command[i] == ' ' && !inCondition);
				else
					temp += command[i];
			}
			command = temp;
			temp = "";
			//strip parenthesis, and single quotes
			for (i = 0; i < command.length(); i++)
				if (command[i] != '\'' && command[i] != '(' && command[i] != ')' && command[i] != ';'){
					temp += command[i];
				}

			entry = new rowdata(StringToUpper(temp));
			if (entry->getLength() != schema->getLength()){
				cout << "Error! Insert data does not match Schema" << endl;
				return -1;
			}
			indexable_t = indexableTree;
			//int sum = 0;
			for (int i = 0; i < schema->indexablecolumns; i++){
				indexable_t->setRow(entry);
				//sum += indexable_t->tree->insertSteps;
				indexable_t = indexable_t->getNext();
			}
			//cout << "Insert Steps: " << sum << endl;
			cout << " 1 record added" << endl;
		}
		//end If INSERT

		if (instruction == "DELETE"){
			bool inCondition = false; //has the program encountered a \' symbol in the where statement?
			temp = "";
			columnNum = 0; //being used as a general int value placeholder throughout the rest of the program
			//get where condition if one exist
			for (i = 0; i < command.length(); i++){
				if ((command[i] == ' ' || command[i] == ';') && !inCondition){
					columnNum++;
					if (columnNum == 2)
						columnToCompare = temp;
					else if (columnNum == 4){
						for (int j = 0; j < temp.length(); j++)
							condition += temp[j];
					}
					temp = "";
				}
				else if (command[i] == ' ' && inCondition)
					temp += ' ';
				else if (command[i] == '\'')
					inCondition = !inCondition;
				else
					temp += command[i];
			}


			if (columnToCompare != "")
			{
				/*columnNum = schema->findColumnName(columnToCompare);
				indexable_t = indexableTree;
				for (int j = 0; j != columnNum; j++){
				indexable_t = indexable_t->getNext();
				}
				rowdata *temp = NULL;
				vector<rowdata*> sresult;
				sresult = indexable_t->getRows(condition);
				records = sresult.size();
				for (int k = 0; k < sresult.size(); k++){
				temp = sresult[k];
				delete temp;
				}*/

				//columnNum = schema->findColumnName(columnToCompare);
				//indexable_t = indexableTree;
				//while (indexable_t != NULL){
				//	indexable_t->deleteRows(condition, columnNum);
				//	//cout << "Delete steps: " << indexable_t->tree->deleteSteps << endl;
				//	indexable_t = indexable_t->getNext();
				//}

				columnNum = schema->findColumnName(columnToCompare);
				indexable_t = indexableTree;
				while (indexable_t != NULL){
					indexable_t->deleteRows(condition, columnNum);
					//cout << "Delete steps: " << indexable_t->tree->deleteSteps << endl;
					indexable_t = indexable_t->getNext();
				}
			}
		}
		//end If DELETE
	}

	t2 = clock();
	cout << "Statement executed in " << ((float)t2 - (float)t1) << "ms" << endl << endl;
	return ((float)t2 - (float)t1);
}

BDatabase::~BDatabase(){
	delete schema;
}

string BDatabase::StringToUpper(string strToConvert)
{
	std::transform(strToConvert.begin(), strToConvert.end(), strToConvert.begin(), ::toupper);

	return strToConvert;
}