#include "avlTree.h"

// A utility function to get height of the tree
int AVLTreeType::height(struct node *N)
{
	if (N == NULL)
		return 0;
	return N->height;
}

/* Helper function that allocates a new node with the given key and
NULL left and right pointers. */
struct node* AVLTreeType::newNode(rowdata* row)
{
	struct node* node = (struct node*)
		malloc(sizeof(struct node));
	node->key = row;
	node->left = NULL;
	node->right = NULL;
	node->height = 1;  // new node is initially added at leaf
	return(node);
}

// A utility function to right rotate subtree rooted with y
// See the diagram given above.
struct node *AVLTreeType::rightRotate(struct node *y)
{
	if (y->left != NULL){
		struct node *x = y->left;
		struct node *T2 = x->right;

		// Perform rotation
		x->right = y;
		y->left = T2;

		// Update heights
		y->height = max(height(y->left), height(y->right)) + 1;
		x->height = max(height(x->left), height(x->right)) + 1;

		// Return new root
		return x;
	}
	return y;
}

// A utility function to left rotate subtree rooted with x
// See the diagram given above.
struct node *AVLTreeType::leftRotate(struct node *x)
{
	if (x->right != NULL){
		struct node *y = x->right;
		struct node *T2 = y->left;

		// Perform rotation
		y->left = x;
		x->right = T2;

		//  Update heights
		x->height = max(height(x->left), height(x->right)) + 1;
		y->height = max(height(y->left), height(y->right)) + 1;

		// Return new root
		return y;
	}
	else return x;
}

// Get Balance factor of node N
int AVLTreeType::getBalance(struct node *N)
{
	if (N == NULL)
		return 0;
	return height(N->left) - height(N->right);
}

struct node* AVLTreeType::insert(struct node* node, rowdata* key)
{
	/* 1.  Perform the normal BST rotation */
	if (node == NULL)
		return(newNode(key));

	if (key->getRowData(colNum) < node->key->getRowData(colNum))
		node->left = insert(node->left, key);
	else if (key->getRowData(colNum) > node->key->getRowData(colNum))
		node->right = insert(node->right, key); 
	else{
		//values are the same
		if (node->key->getRowData(colNum) == key->getRowData(colNum)){
			node->key->duplicateEntry.push_back(key);
		}
	}

	node->height = max(height(node->left), height(node->right)) + 1;

	/* 3. Get the balance factor of this ancestor node to check whether
	this node became unbalanced */
	int balance = getBalance(node);

	// If this node becomes unbalanced, then there are 4 cases

	// Left Left Case
	if (balance > 1 && key->getRowData(colNum) < node->left->key->getRowData(colNum))
		return rightRotate(node);

	// Right Right Case
	if (balance < -1 && key->getRowData(colNum) > node->right->key->getRowData(colNum))
		return leftRotate(node);

	// Left Right Case
	if (balance > 1 && key->getRowData(colNum) > node->left->key->getRowData(colNum))
	{
		node->left = leftRotate(node->left);
		return rightRotate(node);
	}

	// Right Left Case
	if (balance < -1 && key->getRowData(colNum) < node->right->key->getRowData(colNum))
	{
		node->right = rightRotate(node->right);
		return leftRotate(node);
	}

	/* return the (unchanged) node pointer */
	return node;
}

/* Given a non-empty binary search tree, return the node with minimum
key value found in that tree. Note that the entire tree does not
need to be searched. */
struct node * AVLTreeType::minValueNode(struct node* node)
{
	struct node* current = node;

	/* loop down to find the leftmost leaf */
	while (current->left != NULL)
		current = current->left;

	return current;
}

struct node* AVLTreeType::deleteNode(struct node* root, string Searchkey, int col)
{
	// STEP 1: PERFORM STANDARD BST DELETE

	if (root == NULL)
		return root;

	// If the key to be deleted is smaller than the root's key,
	// then it lies in left subtree
	if (Searchkey < root->key->getRowData(col))
		root->left = deleteNode(root->left, Searchkey,col);

	// If the key to be deleted is greater than the root's key,
	// then it lies in right subtree
	else if (Searchkey > root->key->getRowData(col))
		root->right = deleteNode(root->right, Searchkey,col);

	// if key is same as root's key, then This is the node
	// to be deleted
	else
	{
		// node with only one child or no child
		if ((root->left == NULL) || (root->right == NULL))
		{
			struct node *temp = root->left ? root->left : root->right;

			// No child case
			if (temp == NULL)
			{
				temp = root;
				root = NULL;
			}
			else // One child case
				*root = *temp; // Copy the contents of the non-empty child

			delete(temp);
		}
		else
		{
			// node with two children: Get the inorder successor (smallest
			// in the right subtree)
			struct node* temp = minValueNode(root->right);

			// Copy the inorder successor's data to this node
			root->key = temp->key;

			// Delete the inorder successor
			root->right = deleteNode(root->right, temp->key->getRowData(col),col);
		}
	}

	// If the tree had only one node then return
	if (root == NULL)
		return root;

	// STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
	root->height = max(height(root->left), height(root->right)) + 1;

	// STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
	//  this node became unbalanced)
	int balance = getBalance(root);

	// If this node becomes unbalanced, then there are 4 cases

	// Left Left Case
	if (balance > 1 && getBalance(root->left) >= 0)
		return rightRotate(root);

	// Left Right Case
	if (balance > 1 && getBalance(root->left) < 0)
	{
		root->left = leftRotate(root->left);
		return rightRotate(root);
	}

	// Right Right Case
	if (balance < -1 && getBalance(root->right) <= 0)
		return leftRotate(root);

	// Right Left Case
	if (balance < -1 && getBalance(root->right) > 0)
	{
		root->right = rightRotate(root->right);
		return leftRotate(root);
	}

	return root;
}

vector<rowdata*> AVLTreeType::search(node *root, string searchKey, int col)
{
	string watch;
	bool found = false;
	node *current;
	vector<rowdata*> res(0); 
	vector<rowdata*> temp;
	rowdata *data;
	vector<string> alreadyTaken;

	if (root == NULL)
		cerr << "Cannot search the empty tree." << endl;
	else
	{
		current = root;

		while (current != NULL && !found)
		{
			watch = current->key->getRowData(col);
			if (current->key->getRowData(col) == searchKey){
				res.push_back(current->key);
				temp = current->key->duplicateEntry;
				for (int i = 0; i < temp.size(); i++){
					data = temp.at(i);
					if (data->getRowData(col) == searchKey){
						if (!isAlreadyTaken(alreadyTaken, data->getRowData(0)))
						res.push_back(temp[i]);
						alreadyTaken.push_back(data->getRowData(0));
					}
				}
				found = true;
				return res;
			}
			else
				if (current->key->getRowData(col) > searchKey)
					current = current->left;
				else
					current = current->right;
		}//end while
	}//end else

	return res;
}

bool AVLTreeType::isAlreadyTaken(vector<string> test, string compareItem){
	for (int i = 0; i < test.size(); i++)
		if (test[i] == compareItem)
			return true;
	return false;
}