#include "rowdata.h"

int rowdata::getLength(){
	DataEntry *entry = row;
	int count = 0;
	if (row == NULL)
		return 0;
	while (entry->getNext() != NULL){
		count++;
		entry = entry->getNext();
	}
	return count;
}

rowdata::rowdata(string data){
	StringSplit split(50, ',');
	split.newString(data);
	DataEntry *entry = NULL;

	for (int i = 0; i < split.tokens; i++){
		if (row != NULL && entry != NULL){
			entry->setNext(new DataEntry(i, split.tokenList[i]));
			entry = entry->getNext();
		}
		else{
			row = new DataEntry(i, split.tokenList[i]);
			entry = row;
		}
	}
}

string rowdata::getRowData(int key){
	DataEntry *entry;
	int test;
	assert(row != NULL); //row cannot be null
	entry = row;
	do {
		test = entry->getKey();
		if (test == key){
			return entry->getValue();
		}
		entry = entry->getNext();
	} while (entry != NULL);

	return "";
}
rowdata::~rowdata(){
	DataEntry *prevEntry;
	while (row != NULL){
		prevEntry = row;
		row = row->getNext();
		delete prevEntry;
	}
}