#if !defined (DATA_H)
#define DATA_H
#include <string>
using namespace std;

class DataEntry{
private:
	DataEntry *next;
	string value;
	int key;
public:
	int getKey();
	void setKey(int);
	void setValue(string);
	string getValue();
	DataEntry(int, string);
	void setNext(DataEntry *next);
	DataEntry *getNext();
};
#endif