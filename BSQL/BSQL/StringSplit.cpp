#include "StringSplit.h"

StringSplit::StringSplit(int bufferSize, char delimiter){
	buffSize = bufferSize;
	specialChar = delimiter;
}

void StringSplit::newString(string input){
	tokenList.clear();
	tokenList.resize(buffSize, "");
	tokens = 0;
	string word = "";

	for (int i = 0; i < input.length(); i++){
		if (input[i] == specialChar){
			tokenList[tokens] = word;
			tokens++;
			word = "";
		}
		else{
			word += input[i];
		}
	}
	if (word != ""){
		tokenList[tokens] = word;
		tokens++;
	}
}