#if !defined (STRINGS_H)
#define STRINGS_H
#include <string>
#include <vector>
using namespace std;
class StringSplit{
private:
	int buffSize;
	char specialChar = ',';
public:
	void newString(string input);
	vector<string> tokenList;
	StringSplit(int bufferSize, char delimiter);
	int tokens = 0;
};

#endif