#include "DataEntry.h"

DataEntry::DataEntry(int k, string s){
	this->key = k;
	this->next = NULL;
	this->value = s;
}

int DataEntry::getKey(){
	return key;
}
void DataEntry::setNext(DataEntry *n){
	this->next = n;
}

DataEntry* DataEntry::getNext(){
	return this->next;
}

void DataEntry::setKey(int k){
	this->key = k;
}

void DataEntry::setValue(string s){
	this->value = s;
}

string DataEntry::getValue(){
	return this->value;
}