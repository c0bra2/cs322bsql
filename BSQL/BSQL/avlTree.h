#if !defined(H_AVLTree)
#define H_AVLTree
#include "rowdata.h"
#include <iostream>
#include "Schema.h"
#include <vector>

using namespace std;

// An AVL tree node
struct node
{
	rowdata *key;
	struct node *left;
	struct node *right;
	int height;
};

class AVLTreeType
{
public:
	int colNum;
	string searchKey;
	struct node* newNode(rowdata* row);
	struct node *rightRotate(struct node *y);
	struct node *leftRotate(struct node *x);
	int getBalance(struct node *N);
	struct node* insert(struct node* node, rowdata* key);
	struct node * minValueNode(struct node* node);
	struct node* deleteNode(struct node* root, string Searchkey, int col);
	vector<rowdata*> search(node *root, string searchKey, int col);
	bool isAlreadyTaken(vector<string> test, string compareItem);
	int height(struct node*);
};

#endif