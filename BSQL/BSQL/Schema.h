#if !defined (SCHEMA_H)
#define SCHEMA_H
#include "DataEntry.h"
#include "StringSplit.h"
#include <algorithm>
#include <fstream>
class Schema{
private:
	DataEntry *Column_Name = NULL;
	DataEntry *Data_Type = NULL;
	DataEntry *Is_Index = NULL;
	bool tableFound = true;
	string StringToUpper(string strToConvert);
public:
	int getLength();
	int columnNum = 0;
	Schema(string TABLENAME);
	~Schema();
	int indexablecolumns = 0;
	int findColumnName(string columnName);
	string findColumnName(int columnIndex); //find and return the string name of a column by searching index. negative if not found
	string findDataType(int columnIndex); //find and return the datatype of a column by searching index
	string findIsIndex(int); //find and return if this column is indexable by searching index
	bool TableFound(); //returns a bool as to whether the tablename was successfully found and read or not.
}; 
#endif