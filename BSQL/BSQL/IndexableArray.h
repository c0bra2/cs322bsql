#if !defined (INDEXABLE_H)
#define INDEXABLE_H
#include <vector>
#define LARGEP 6
#include <iostream>
#include "rowdata.h"
using namespace std;

class IndexableArray{
private:
	int key;
	int ARRAYSIZE = 7713;
	int currentPrime = 0;
	int largePrimes[6];
	int entries = 0;
	IndexableArray *next;
	vector<rowdata*> table;
	//rowdata **table = new rowdata*[ARRAYSIZE];
public:
	void resizeHashTable();
	int getKey();
	long reHash(char *key);
	IndexableArray *getNext();
	rowdata *getRow(int);
	float getLoadFactor();
	void setRow(int, rowdata*);
	void setNext(IndexableArray *next);
	int getDataSize();
	~IndexableArray();
	IndexableArray(int);
};
#endif