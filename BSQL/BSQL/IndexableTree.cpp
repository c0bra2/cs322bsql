#include "IndexableTree.h"

IndexableTree::IndexableTree(int k){
	this->key = k;
	this->next = NULL;
	this->tree->colNum = k;
}

int IndexableTree::getKey(){
	return key;
}

void IndexableTree::deleteRows(string searchKey, int columnNum)
{
	root = tree->deleteNode(root,searchKey,columnNum);
}

//void IndexableTree::selectALL(Schema* schobj, vector<string> columns)
//{
//	tree->inorderTraversal(schobj, columns);
//}

vector<rowdata*> IndexableTree::getRows(string searchKey){
	return tree->search(root,searchKey, key);
}

void IndexableTree::setRow(rowdata *row){
	root = tree->insert(root, row);
}

void IndexableTree::setNext(IndexableTree *n){
	this->next = n;
}

IndexableTree* IndexableTree::getNext(){
	return this->next;
}

IndexableTree::~IndexableTree(){
	//CLEANUP

}